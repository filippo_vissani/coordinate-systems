Progetto utilizzato per testare la gestione dei sistemi di coordinate.
Nel progetto sono presenti:
- un cubo ancorato alla mesh e un rover come figlio, il rover ha i seguenti componenti:
Object Manipulator
Spatial Mapping Collider
RigidBody
- Un cubo disposto su un piano, al quale viene applicata la forza di gravità, una volta spostato dal piano cadrà sulla mesh.
- Un cubo non ancorato alla mesh con una sfera come figlio che gli ruota attorno.
spostando il cubo la sfera continuerà a girargli intorno (in base al suo sistema di coordinate).