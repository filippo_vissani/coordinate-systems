﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Persistence;

public class AnchorManager : MonoBehaviour
{
    public GameObject rootGameObject;
    private WorldAnchorStore worldAnchorStore;
    private bool savedRoot;

    // Start is called before the first frame update
    void Start()
    {
        WorldAnchorStore.GetAsync(StoreLoaded);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void StoreLoaded(WorldAnchorStore store)
    {
        worldAnchorStore = store;
        LoadAnchor();
        SaveAnchor();
    }

    public void DeleteExistingAnchor()
    {
        WorldAnchor anchor = rootGameObject.GetComponent<WorldAnchor>();
        if (anchor != null)
        {
            Destroy(anchor);
            worldAnchorStore.Delete(rootGameObject.name);
            this.savedRoot = false;
        }
    }

    public void SaveAnchor()
    {
        WorldAnchor anchor = rootGameObject.AddComponent<WorldAnchor>();
        if (!this.savedRoot && anchor != null)
        {
            this.savedRoot = this.worldAnchorStore.Save(rootGameObject.name, anchor);
        }
    }

    private void LoadAnchor()
    {
        this.savedRoot = this.worldAnchorStore.Load(rootGameObject.name, rootGameObject);
    }
}
